<!DOCTYPE html>
<html lang="nl">
<head>
    <meta charset="utf-8">
    <title>Intro evaluatie</title>
    <style>
        p {
            margin: 0;
        }

        form {
            gap: 10px;
            margin-top: 10px;
            display: flex;
            align-items stretch;
        }
    </style>
</head>
<body>
<?php

?>
<p>
    Beste student, je hebt meegedaan aan de introductie. We willen graag weten wat je daar van vond. Vul je algemene indruk in, geef de introductie een rapportcijfer en vul tenslotte je leeftijd in. Verstuur daarna het formulier.
</p>
<form method="POST" action="resultaat.php">
    <div>
        <label>Rapportcijfer: </label><input type="text" name="rapportcijfer"><br>
        <label>Leeftijd: </label><input type="text" name="leeftijd"><br>
    </div>

    <div>
        <p>Algemene indruk</p>
        <input type="radio" id="super" name="indruk" value="Super">
        <label for="super">Super</label><br>
        <input type="radio" id="goed" name="indruk" value="Goed">
        <label for="goed">Goed</label><br>
        <input type="radio" id="matig" name="indruk" value="Matig">
        <label for="matig">Matig</label><br>
        <input type="radio" id="slecht" name="indruk" value="Slecht">
        <label for="slecht">Slecht</label>
    </div>

    <input type="submit" value="Verstuur!">
</form>
</body>
</html>