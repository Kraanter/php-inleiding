<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Controleer</title>
    <style>
        body {
            font-family: "Abadi MT Condensed Extra Bold";
        }
        h1 {
            color: red;
        }
    </style>
</head>
<body>
<?php
    $data = $_POST;
    $leeftijd = null;
    $naam = null;

    function leeftijd($leeftijd, $naam) {
        if($leeftijd < 18) {
            $lengte = 18 - $leeftijd;
            return "Over ". $lengte ." jaar is ". $naam ." volwassen.";
        } elseif ($leeftijd <= 65 && $leeftijd >= 21) {
            return $naam. " is nu eigenlijk pas echt volwassen.";
        }
        return false;
    }

    print(($data["naam"] ?? "") == "" ? "<h1>Naam is verplicht</h1>" : !($naam = $data["naam"]));
    print(($data["leeftijd"] ?? "") == "" ? "<h1>Leeftijd is verplicht</h1>" : !($leeftijd = strval($data["leeftijd"])));

    if($naam === null || $leeftijd === null) {
        print("<input type='button' value='Ga terug!' onclick='location.href=\"formulier.php\"'>");
    } else {
        if($leeftijd < 0){
            print("<h1>Leeftijd moet Groter of gelijk zijn aan 0.</h1>");
        } else {
            print("<h2>". leeftijd($leeftijd, $naam) ."</h2>");
        }
    }


?>
</body>
</html>