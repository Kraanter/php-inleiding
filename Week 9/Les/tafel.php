<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        body {
            display: flex;
            align-content: baseline;
            gap: 3%;
            justify-content: center;

        }
    </style>
</head>
<body>

<div>
    <form method="get" action="tafel.php">
        <h2>Maak je keuze voor een tafel:</h2>
        <?php
        for($getal = 1; $getal <= 10; $getal++){
            print("<input type='radio' name='tafel' value='". $getal ."'>".$getal."<br>");
        }

        $keuzes = Array("Goed", "Matig", "Slecht");

        ?>
        <hr>
        <input type="submit" value="Toon">
    </form>
</div>

<div>
    <h2>Je antwoord:</h2>
    <?php
        if(isset($_GET["tafel"])){
            for($getal = 1; $getal <= 10; $getal++){
                print("<span>".$getal." X ". $_GET["tafel"] . " = ". $getal*$_GET["tafel"]."</span><br>");
            }

            print("</div><div>");

            print("<h2>Wat vond je van de lijst?</h2>");

            foreach($keuzes as $keuze) {
                print("<input type='radio' name='keuze' value='". $keuze ."'>".$keuze."<br>");
            }

        }
    ?>
</div>

</body>
</html>
