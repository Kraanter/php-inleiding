<!DOCTYPE html>
<html lang="nl">
<head>
    <meta charset="utf-8">
    <title>resultaat</title>
    <style>
        html {
            font-family: "Abadi MT Condensed Extra Bold";
        }

        h2 {
            margin: 0px;
        }

        h1 {
            color: red;
        }
    </style>
</head>
<body>
<?php
    $data = $_POST;

    $cijfer = $data["rapportcijfer"] ?? "";
    $leeftijd = $data["leeftijd"] ?? "";
    $indruk = $data["indruk"] ?? "";

    if(($cijfer == "" || $leeftijd == "" || $indruk == "")){
        print($cijfer == "" ? "<h1>Geen rapportcijfer ingevuld</h1>" : null);
        print($leeftijd == "" ? "<h1>Geen leeftijd ingevuld</h1>" : null);
        print($indruk == "" ? "<h1>Geen algemene indruk ingevuld</h1>" : null);
    } else {
        if (!($cijfer >= 1 && $cijfer <= 10) || !($leeftijd >= 16 && $leeftijd <= 120)) {
            print("<h1>Hey, een rapportcijfer moet tussen 1 en 10 liggen en leeftijd tussen 16 en 120!</h2>");
        } else {
            print("<h2>Je geeft de intro een ".$cijfer."</h2>");
            print("<h2>Je bent volgens je eigen zeggen ".$leeftijd." jaar oud</h2>");
            print("<h2>Je vond de algemene indruk ".$indruk."</h2>");
        }
        print("<a href='rapport.php'>Terug naar het evaluatieformulier...</a>");
    }

?>



</body>
</html>