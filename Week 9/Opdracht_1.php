<!DOCTYPE html>
<html lang="nl">
<head>
    <meta charset="utf-8">
    <title>Regelen</title>
</head>
<body>
<?php
$temperaturen = array();
$temperaturen["Assen"] = 14.6;
$temperaturen["Maastricht"] = 17.4;
$temperaturen["Zwolle"] = 15.2;
$temperaturen["Amsterdam"] = 13.6;
?>
    <table>
        <tr>
            <th>Plaats</th>
            <th>Temperatuur</th>
        </tr>
        <?php
        foreach ($temperaturen as $plaats => $temperatuur){
            print("<tr>");
            print("<td>".$plaats."</td>");
            print("<td>".$temperatuur."</td>");
            print("</tr>");
        }
        print_r($_GET);
        print_r($_POST);
        ?>
    </table>
</body>