<!DOCTYPE html>
<!--[Krijn Grimme  S1171837]-->
<html>
    <head>
        <meta charset="utf-8">
        <title>Opgave 11</title>
    </head>
    <body>
        <h1>Hoofdstuk</h1>
        <?php

        /* Gebruik onderstaande variabelen in de uitwerking */
        $aantalHoofdstukken = 8;

        /* Begin uitwerking */

        for($i = 1; $i <= $aantalHoofdstukken; $i++) {
            print("<h1>Hoofdstuk ". $i . "</h1>");
        }

        /* Einde uitwerking */

        ?>
    </body>
</html>