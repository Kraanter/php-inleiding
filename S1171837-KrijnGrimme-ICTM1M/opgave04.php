<!DOCTYPE html>
<!--[Krijn Grimme  S1171837]-->
<html>
    <head>
        <meta charset="utf-8">
        <title>Opgave 4</title>
    </head>
    <body>
        <h1>Opgave 4</h1>

        <?php

        /* Gebruik onderstaande variabelen in de uitwerking */
        $restwaarde = 5000;
        $dagwaarde = 12000;
        $reparatiekosten = 6000;

        // gebruik onderstaande regels in je uitwerking voor het printen van de juiste output:
        //
        // "total loss"
        // "reparatie"

        /* Begin uitwerking */

        if(($dagwaarde - $restwaarde) < $reparatiekosten) {
            print("total loss");
        }  else {
            print("reparatie");
        }

        /* Einde uitwerking */

        ?>
    </body>
</html>