<!DOCTYPE html>
<!--[Krijn Grimme  S1171837]-->
<html>
    <head>
        <meta charset="utf-8">
        <title>Opgave 3</title>
    </head>
    <body>
        <h1>Opgave 3</h1>

        <?php

        /* Gebruik onderstaande variabelen in de uitwerking */
        $lengte = 165; // lengte in centimeters
        $aantalKeerMetLift = 3;

        // gebruik onderstaande regels in je uitwerking voor het printen van de juiste output:
        //
        // "koffie!"
        // "helaas"

        /* Begin uitwerking */

        if (($lengte != 170) && (($lengte < 170 && $aantalKeerMetLift <= 2) || ($lengte > 170 && $aantalKeerMetLift <= 3))) {
            print("koffie!");
        } else {
            print("helaas");
        }
        /* Einde uitwerking */

        ?>
    </body>
</html>