<!DOCTYPE html>
<!--[Krijn Grimme  S1171837]-->
<html>
    <head>
        <meta charset="utf-8">
        <title>Opgave 6</title>
    </head>
    <body>
        <h1>Opgave 6</h1>

        <?php
        /* Gebruik onderstaande variabele in de uitwerking */
        $land = "Frankrijk";

        // gebruik onderstaande regel in je uitwerking voor het printen van de juiste output:
        //
        // "De hoofdstad van ... is ..."

        /* Begin uitwerking */

        $hoofdsteden = Array("Duitsland" => "Berlijn", "Frankrijk" => "Parijs", "Spanje" => "Madrid", "Japan" => "Tokio");

        print("De hoofdstad van ". $land . " is " . $hoofdsteden[$land])

        /* Einde uitwerking */

        ?>
    </body>
</html>