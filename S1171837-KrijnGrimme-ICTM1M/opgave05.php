<!DOCTYPE html>
<!--[Krijn Grimme  S1171837]-->
<html>
    <head>
        <meta charset="utf-8">
        <title>Opgave 5</title>
    </head>
    <body>
        <h1>Opgave 5</h1>

        <?php
        /* Gebruik onderstaande variabelen in de uitwerking */
        $temperatuur = 9;
        $zomer = false;

        // gebruik onderstaande regels in je uitwerking voor het printen van de juiste output:
        //
        // "koud"
        // "normaal"
        // "warm"

        /* Begin uitwerking */

        if(!$zomer) {
            if($temperatuur <= 10) {
                print("koud");
            } elseif($temperatuur <= 15) {
                print("normaal");
            } else {
                print("warm");
            }
        } else {
            if($temperatuur <= 15) {
                print("koud");
            } elseif($temperatuur <= 20) {
                print("normaal");
            } else {
                print("warm");
            }
        }

        /* Einde uitwerking */

        ?>
    </body>
</html>