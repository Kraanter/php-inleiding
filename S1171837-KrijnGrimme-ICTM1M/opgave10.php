<!DOCTYPE html>
<!--[Krijn Grimme  S1171837]-->
<html>
    <head>
        <meta charset="utf-8">
        <title>Opgave 10</title>
    </head>
    <body>
        <h1>Opgave 10</h1>

        <?php

        /* Begin uitwerking */

        function schreew($string) {
            return (strtoupper($string) . "!");
        }


        $tekst = schreew("Snoep gezond, eet een appel");

        print($tekst);

        /* Einde uitwerking */

        ?>
    </body>
</html>