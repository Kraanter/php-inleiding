<!DOCTYPE html>
<!--[Krijn Grimme  S1171837]-->
<html>
    <head>
        <meta charset="utf-8">
        <title>Opgave 9</title>
    </head>
    <body>
        <h1>Opgave 9</h1>

        <?php

        /* Gebruik onderstaande variabelen in de uitwerking */
        $rentePercentage = -3;
        $startKapitaal = 100;

        /* Begin uitwerking */

        $nieuwKapitaal = $startKapitaal;

        $aantal = 0;

        while($startKapitaal / 2 < $nieuwKapitaal) {
            $nieuwKapitaal = $nieuwKapitaal + $nieuwKapitaal * ($rentePercentage/100);
            $aantal += 1;
        }

        print($aantal);

        /* Einde uitwerking */

        ?>
    </body>
</html>