<!DOCTYPE html>
<!--[Krijn Grimme  S1171837]-->
<html>
    <head>
        <meta charset="utf-8">
        <title>Opgave 12</title>
    </head>
    <body>
        <h1>Opgave 12</h1>

        <?php
        /* Begin uitwerking */

        $selectie = "";

        if(isset($_POST["beoordeling"])){
            $selectie = $_POST["beoordeling"];
        }

        ?>

        <form method="post" action="opgave12.php">
            <label for="beoordeling">Beoordeling:</label>
            <select id="beoordeling" name="beoordeling" required>
                <option value="">Selecteer een beoordeling</option>
                <option value="slecht" <?php
                    if($selectie == "slecht") {print("selected");};
                ?>>Slecht</option>
                <option value="matig"<?php
                    if($selectie == "matig") {print("selected");};
                ?>>Matig</option>
                <option value="gemiddeld"<?php
                    if($selectie == "gemiddeld") {print("selected");};
                ?>>Gemiddeld</option>
                <option value="goed"<?php
                    if($selectie == "goed") {print("selected");};
                ?>>Goed</option>
                <option value="uitstekend"<?php
                    if($selectie == "uitstekend") {print("selected");};
                ?>>Uitstekend</option>
            </select>
            <br><br>
            <input type="submit" name="submit" value="Verzenden">
        </form>

        <?php

        if($selectie != "") {
            print("<br>Je beoordeling is ontvangen!");
        }

        /* Einde uitwerking */
        ?>

    </body>
</html>