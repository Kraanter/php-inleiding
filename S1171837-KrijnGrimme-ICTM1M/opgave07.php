<!DOCTYPE html>
<!--[Krijn Grimme  S1171837]-->
<html>
    <head>
        <meta charset="utf-8">
        <title>Opgave 7 a + b</title>
    </head>
    <body>
        <h1>Opgave 7 a + b</h1>

        <?php

        // gebruik onderstaande regels in je uitwerking voor het printen van de juiste output:
        //
        // "puntenTotaal:"
        // "aantalGewonnen:"
        // "aantalGelijk:"
        // "aantalVerloren:"

        /* Begin uitwerking a + b */

        $uitslagen = Array(1 => 3, 2 => 1, 3 => 3, 4 => 0, 5 => 0, 6 => 3);

        $totaal = 0;
        $gewonnen = 0;
        $gelijk = 0;
        $verloren = 0;

        foreach ($uitslagen as $item => $score) {
            $totaal += $score;
            if($score === 3) {
                $gewonnen += 1;
            } elseif ($score === 1) {
                $gelijk += 1;
            } elseif ($score === 0) {
                $verloren += 1;
            }
        }

        print("puntenTotaal: ".$totaal);
        print("<br>aantalGewonnen: ".$gewonnen);
        print("<br>aantalGelijk: ".$gelijk);
        print("<br>aantalVerloren: ".$verloren);

        /* Einde uitwerking a + b */

        ?>
    </body>
</html>