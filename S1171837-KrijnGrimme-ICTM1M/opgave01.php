<!DOCTYPE html>
<!--[Krijn Grimme  S1171837]-->
<html>
    <head>
        <meta charset="utf-8">
        <title>Opgave 1</title>
    </head>
    <body>
        <h1>Opgave 1</h1>

        <?php

        /* Gebruik onderstaande variabelen in de uitwerking */
        $zoekterm = "pad";
        $tekenreeks = "schildpad";

        /* Begin uitwerking */
        print(strpos($tekenreeks, $zoekterm));


        /* Einde uitwerking */

        ?>
    </body>
</html>