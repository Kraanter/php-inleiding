<!DOCTYPE html>
<!--[Krijn Grimme  S1171837]-->
<html>
    <head>
        <meta charset="utf-8">
        <title>Opgave 8 a + b</title>
    </head>
    <body>
        <h1>Opgave 8 a + b</h1>

        <?php

        /* Gebruik onderstaande variabelen in de uitwerking */
        $grootste = 4;
        $kleinste = 0;

        /* Begin uitwerking */

        // Opdracht A

        for($i = $grootste; $i >= $kleinste; $i--){
            print($i. " ");
        }

        print("<br>");

        // Opdracht B

        for($i = $grootste; $i >= $kleinste; $i--){
            if($i % 2 != 0) {
                print($i. " ");
            }
        }

        /* Einde uitwerking */

        ?>
    </body>
</html>