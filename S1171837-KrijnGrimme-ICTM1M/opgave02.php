<!DOCTYPE html>
<!--[Krijn Grimme  S1171837]-->
<html>
    <head>
        <meta charset="utf-8">
        <title>Opgave 2</title>
    </head>
    <body>
        <h1>Opgave 2</h1>

        <?php

        /* Gebruik onderstaande variabelen in de uitwerking */
        $getal1 = 4.3;
        $getal2 = 6.9;

        /* Begin uitwerking */

        print(floor(min($getal1, $getal2)));

        /* Einde uitwerking */

        ?>
    </body>
</html>