<?php

function printCitaat($citaat) {
    return "--- ". ucfirst($citaat) . " ---\n";
}

function printCitaat2($acteur, $citaat) {
    return "--- ". ucfirst($acteur) .": ". ucfirst($citaat) . " ---\n";
}

print(printCitaat("To be or not to be"));

print(printCitaat2("Julius Caesar","Alea iacta est"));