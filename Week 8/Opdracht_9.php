<?php

function berekenAantalTegels($muurBreedte, $muurHoogte, $tegelBreedte, $tegelHoogte) {
    $muur = $muurHoogte * $muurBreedte;
    $tegel = $tegelBreedte * $tegelHoogte;
    $aantal = $muur / $tegel;

    return ceil($aantal);
}

function berekenAantalDozen($muurBreedte, $muurHoogte, $tegelBreedte, $tegelHoogte, $doosInhoud) {
    $aantalTegels = berekenAantalTegels($muurBreedte, $muurHoogte, $tegelBreedte, $tegelHoogte);
    $aantal = $aantalTegels / $doosInhoud;
    return ceil($aantal);
}


$aantalDozen = berekenAantalDozen(5, 2.6, 0.15, 0.15, 20);
print("Voor deze muur moet je " . $aantalDozen . " dozen kopen.\n");
