<?php

function printCitaat($citaat) {
    print("--- ". $citaat . " ---\n");
}

function printCitaat2($acteur, $citaat) {
    print("--- ". $acteur .": ". $citaat . " ---\n");
}

printCitaat("To be or not to be");

printCitaat2("Julius Caesar","Alea iacta est");