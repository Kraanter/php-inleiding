<?php

function printMail($naam, $dag, $isVerstuurd) {
    if($dag === "") {
        $dag = "vandaag";
    }
    if($isVerstuurd) {
        $str = "Je pakket is ". $dag ." verzonden.";
    } else {
        $str = "Je pakket wordt ". $dag ." verzonden";
    }
    print("Beste " . $naam . ", \n". $str ." \n\nVriendelijke groeten,\nSnelpakket.nl\n");
}

printMail("Onno de Vries", "", TRUE);
printMail("Ansu Fati", "", FALSE);