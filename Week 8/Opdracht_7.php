<?php

$hoofdstukken = array(
    "Voorwoord" => 5,
    "Inleiding" => 7,
    "Expressionisme" => 9,
    "Surrealisme" => 29,
    "Kubisme" => 53,
    "Dada" => 66,
    "Futurisme" => 90,
    "Nieuwe zakelijkheid" => 99,
    "De Stijl" => 121,
    "Cobra" => 144,
    "Literatuur" => 158);

function berekenAantalPuntjes($woord1, $woord2) {
    return 50 - strlen($woord1) - strlen($woord2);
}

function printHoofdstuk($titel, $pagina) {
    $aantalPuntjes = berekenAantalPuntjes($titel, $pagina);
    $puntjes = str_repeat(".", $aantalPuntjes);

    print($titel . $puntjes . $pagina. "\n");
}

foreach ($hoofdstukken as $titel => $pagina) {
    printHoofdstuk($titel, $pagina);
}