<?php

function isSpiegelwoord($woord) {
    if(strrev($woord) === $woord) {
        return true;
    } else {
        return false;
    }
}

function testSpiegelwoord($woord) {
    if(isSpiegelwoord($woord)) {
        print($woord. " is een spiegelwoord");
    } else {
        print($woord. " is geen spiegelwoord");
    }
}

function zoekSpiegelwoorden($woorden, $switch) {
    $spiegelWoorden = Array();
    foreach ($woorden as $i) {
        if(isSpiegelwoord($i) === $switch){
            array_push($spiegelWoorden, $i);
        }
    }
    return $spiegelWoorden;
}

function testZoekSpiegelwoorden($woorden, $switch) {
    print("De volgende woorden zijn spiegelwoorden:");
    $spiegelwoorden = zoekSpiegelwoorden($woorden, $switch);
    foreach ($spiegelwoorden as $i) {
        print("\n". $i);
    }
}

$woorden = array("lepel", "vork", "negen", "tien");
testZoekSpiegelwoorden($woorden, false);