<?php

function tekenRondje() {
    print("O");
}

function tekenLijn($aantal) {
    for($i = 0; $i < $aantal; $i++){
        tekenRondje();
    }
    print("\n");
}

function tekenRechthoek($hoogte, $breedte) {
    for($i = 0; $i < $hoogte; $i++){
        tekenLijn($breedte);
    }
    print("\n");
}

function tekenVierkant($hoogtebreedte) {
    tekenRechthoek($hoogtebreedte, $hoogtebreedte);
}

function tekenDriehoek($hoogte) {
    for($i = 1; $i <= $hoogte; $i++) {
        tekenLijn($i);
    }
}

function tekenPiramide($hoogte) {
    for($i = 1; $i <= $hoogte; $i++) {
        print(str_repeat(" ", $hoogte-$i));
        tekenLijn($i*2-1);
    }
}

tekenPiramide(5);