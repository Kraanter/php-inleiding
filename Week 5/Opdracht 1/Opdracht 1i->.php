<?php
    $aantal = 100;
/*
    |Opdracht 1i|

for($i = 1; $i <= $aantal; $i++){
    print($i." ");
}

    |Opdracht 1j|

for($i = $aantal; $i > 0; $i--){
    print($i." ");
}

    |Opdracht 1k|

for($i = 1; $i <= $aantal; $i++){
    if($i == 50 || $i == 54){
        continue;
    }
    print($i." ");
}

    |Opdracht 1l|

for($i = 2; $i <= $aantal; $i= $i+2){
    print($i." ");
}

    |Opdracht 1m|

$getal = 0;

for($i = 1; $i <= $aantal; $i++){
    $getal = $getal + $i;
}

print($getal);

    |Opdracht 1n|

$getal = 1;

for($i = 1; $i <= $aantal; $i++){
    $getal = $getal * $i;
}

print($getal);

    |Opdracht 1o|

for($i = 1; $i <= 20; $i++){
    print(rand(0,100). " ");
}

    |Opdracht 1p|

$getal = null;

for($i = 1; $i <= 20; $i++){
    $random = rand(0, 100);
    print($random. " ");
    $getal = $getal + $random;
}

print($getal);

    |Opdracht 1q|

$grootst = null;

for($i = 1; $i <= 20; $i++){
    $random = rand(0, 100);
    print($random." ");
    $grootst = max($random, $grootst);
}

print("\nGrootste getal: ". $grootst);

    |Opdracht 1r|

for($i = 0; $i < 4; $i++){
    for($j = 0; $j < 4; $j++){
        $random = rand(1, 100);
        print($random." ");
    }
    print("\n");
}
*/
