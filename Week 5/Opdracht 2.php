<?php

function kaarten($aantal){
    $som = null;

    for($i = 0; $i < $aantal; $i++){
        $kaart = rand(1, 52);
        $som = $som + $kaart;
        print("\nDe waarde van de kaart is: ". $kaart);
    }
    return $som;
}

function dobbelsteen(){
    global $totaal;
    $dobbel = rand(1, 6);
    print("\nHet gegooide getal is: ". $dobbel);
    if($dobbel % 2 == 0){
        $totaal = $totaal + kaarten($dobbel);
        dobbelsteen();
    } else{
        if($totaal == null) print("\nHelaas er zijn geen punten gescoord.\n");
        else print("\n\nEr zijn totaal ". $totaal ." punten gescoord.\n");
    }
}

dobbelsteen();