<?php
print("Na hoeveel jaar is mijn vermogen verdubbeld bij een bepaalde rente? \n");

print("\nHet startvermogen is: ");
$start = fgets(STDIN);

print("Het rentepercentage is: ");
$rente = fgets(STDIN);

$eindvermogen = $start;
$jaren = 0;

while($eindvermogen < $start * 2){
    $eindvermogen = $eindvermogen * (($rente / 100) +1);
    $jaren++;
}

print("\nHet eindvermogen is: ". $eindvermogen);
print("\nHet aantal jaren is: ". $jaren);